﻿using Amazon;
using Amazon.SQS;
using Amazon.SQS.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;

namespace Astro.RPN.Utilities
{
    public class RPNSQS
    {
        private IAmazonSQS sqs = AWSClientFactory.CreateAmazonSQSClient(RegionEndpoint.APSoutheast1);
        private string sqsQueueURL;
        private string sqsQueueName;

        private ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public RPNSQS(string sqsURL, string sqsName)
        {
            sqsQueueURL = sqsURL;
            sqsQueueName = sqsName;
        }

        public RPNSQS()
        {
            sqsQueueURL = ConfigurationManager.AppSettings["Default_SQS_Queue"];
            sqsQueueName = ConfigurationManager.AppSettings["Default_SQS_Queue_Name"];
        }

        public bool QueueSQS(string messageBody)
        {
            //to do 
            try
            {
                SendMessageRequest sendMessageRequest = new SendMessageRequest();
                sendMessageRequest.QueueUrl = sqsQueueURL;
                sendMessageRequest.MessageBody = messageBody;
                sendMessageRequest.DelaySeconds = 0;
                sqs.SendMessage(sendMessageRequest);

                return true;
            }
            catch (AmazonSQSException ex)
            {
                log.Error("[RPN_SQS] Error encounted: " + ex.Message + ". Status Code: " + ex.StatusCode);
                return false;
            }
            catch (Exception e)
            {
                log.Error("[RPN_SQS] Error encounted: " + e.Message);
                return false;
            }
        }

        public bool QueueWithEncryptedSQS(string messageBody)
        {
            //to do 
            try
            {
                SendMessageRequest sendMessageRequest = new SendMessageRequest();
                sendMessageRequest.QueueUrl = sqsQueueURL;

                SimpleEncryption enc = new SimpleEncryption();
                sendMessageRequest.MessageBody = enc.Encrypt(messageBody);

                sendMessageRequest.DelaySeconds = 0;
                sqs.SendMessage(sendMessageRequest);

                return true;
            }
            catch (AmazonSQSException ex)
            {
                log.Error("[RPN_SQS] Error encounted: " + ex.Message + ". Status Code: " + ex.StatusCode);
                return false;
            }
            catch (Exception e)
            {
                log.Error("[RPN_SQS] Error encounted: " + e.Message);
                return false;
            }
        }

        public SQSResponse ReceiveSQS()
        {
            SQSResponse resp = new SQSResponse();
            try
            {
                ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest();
                receiveMessageRequest.QueueUrl = sqsQueueURL;

                ReceiveMessageResponse receiveMessageResponse = sqs.ReceiveMessage(receiveMessageRequest);

                if (receiveMessageResponse.Messages.Count > 0)
                {
                    foreach (Message message in receiveMessageResponse.Messages)
                    {
                        resp.Body = message.Body;
                        resp.MessageID = message.MessageId;
                        resp.MD5OfBody = message.MD5OfBody;
                        resp.ReceiptHandle = message.ReceiptHandle;
                    }

                    //Delete the SQS message once get the SQS.
                    String messageRecieptHandle = receiveMessageResponse.Messages[0].ReceiptHandle;
                    DeleteMessageRequest deleteRequest = new DeleteMessageRequest();
                    deleteRequest.QueueUrl = sqsQueueURL;
                    deleteRequest.ReceiptHandle = messageRecieptHandle;
                    sqs.DeleteMessage(deleteRequest);

                    //Pause for 1 sec after message deleted.
                    Thread.Sleep(500);
                }
                else
                {
                    //Pause for 30 seconds if no SQS message found
                    Thread.Sleep(30000);
                }
            }
            catch (AmazonSQSException ex)
            {
                log.Error("[RPN_SQS] Amazon Error encounted: " + ex.Message + ". Status Code: " + ex.StatusCode);
                resp = new SQSResponse();
            }
            catch (Exception ex)
            {
                log.Error("[RPN_SQS] General Error encounted: " + ex.Message);
                resp = new SQSResponse();
            }

            return resp;
        }
        public SQSResponse ReceiveDecryptedSQS()
        {
            SQSResponse resp = new SQSResponse();
            try
            {
                ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest();
                receiveMessageRequest.QueueUrl = sqsQueueURL;

                ReceiveMessageResponse receiveMessageResponse = sqs.ReceiveMessage(receiveMessageRequest);

                if (receiveMessageResponse.Messages.Count > 0)
                {
                    foreach (Message message in receiveMessageResponse.Messages)
                    {
                        SimpleEncryption enc = new SimpleEncryption();
                        resp.Body = enc.Decrypt(message.Body);
                        resp.MessageID = message.MessageId;
                        resp.MD5OfBody = message.MD5OfBody;
                        resp.ReceiptHandle = message.ReceiptHandle;
                    }

                    //Delete the SQS message once get the SQS.
                    String messageRecieptHandle = receiveMessageResponse.Messages[0].ReceiptHandle;
                    DeleteMessageRequest deleteRequest = new DeleteMessageRequest();
                    deleteRequest.QueueUrl = sqsQueueURL;
                    deleteRequest.ReceiptHandle = messageRecieptHandle;
                    sqs.DeleteMessage(deleteRequest);

                    //Pause for 1 sec after message deleted.
                    Thread.Sleep(500);
                }
                else
                {
                    //Pause for 30 seconds if no SQS message found
                    Thread.Sleep(30000);
                }
            }
            catch (AmazonSQSException ex)
            {
                log.Error("[RPN_SQS] Amazon Error encounted: " + ex.Message + ". Status Code: " + ex.StatusCode);
                resp = new SQSResponse();
            }
            catch (Exception ex)
            {
                log.Error("[RPN_SQS] General Error encounted: " + ex.Message);
                resp = new SQSResponse();
            }

            return resp;
        }

        //Added new library to pull 10 messages
        public List<SQSResponse> Receive10SQS()
        {
            List<SQSResponse> resp = new List<SQSResponse>();
            try
            {
                ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest();
                receiveMessageRequest.QueueUrl = sqsQueueURL;
                receiveMessageRequest.MaxNumberOfMessages = 10; // set until max messages

                ReceiveMessageResponse receiveMessageResponse = sqs.ReceiveMessage(receiveMessageRequest);

                if (receiveMessageResponse.Messages.Count > 0)
                {
                    foreach (Message message in receiveMessageResponse.Messages)
                    {
                        SQSResponse singleResp = new SQSResponse();
                        singleResp.Body = message.Body;
                        singleResp.MessageID = message.MessageId;
                        singleResp.MD5OfBody = message.MD5OfBody;
                        singleResp.ReceiptHandle = message.ReceiptHandle;

                        resp.Add(singleResp);
                    }

                    //Delete the SQS message once get the SQS.
                    String messageRecieptHandle = receiveMessageResponse.Messages[0].ReceiptHandle;
                    DeleteMessageRequest deleteRequest = new DeleteMessageRequest();
                    deleteRequest.QueueUrl = sqsQueueURL;
                    deleteRequest.ReceiptHandle = messageRecieptHandle;
                    sqs.DeleteMessage(deleteRequest);

                    //Pause for 1 sec after message deleted.
                    Thread.Sleep(500);
                }
                else
                {
                    //Pause for 30 seconds if no SQS message found
                    Thread.Sleep(30000);
                }
            }
            catch (AmazonSQSException ex)
            {
                log.Error("[RPN_SQS] Amazon Error encounted: " + ex.Message + ". Status Code: " + ex.StatusCode);
                resp = new List<SQSResponse>();
            }
            catch (Exception ex)
            {
                log.Error("[RPN_SQS] General Error encounted: " + ex.Message);
                resp = new List<SQSResponse>();
            }

            return resp;
        }
        public List<SQSResponse> Receive10DecryptedSQS()
        {
            List<SQSResponse> resp = new List<SQSResponse>();
            try
            {
                ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest();
                receiveMessageRequest.QueueUrl = sqsQueueURL;
                receiveMessageRequest.MaxNumberOfMessages = 10; // set until max messages

                ReceiveMessageResponse receiveMessageResponse = sqs.ReceiveMessage(receiveMessageRequest);

                if (receiveMessageResponse.Messages.Count > 0)
                {
                    foreach (Message message in receiveMessageResponse.Messages)
                    {
                        SQSResponse singleResp = new SQSResponse();
                        SimpleEncryption enc = new SimpleEncryption();

                        singleResp.Body = enc.Decrypt(message.Body);
                        singleResp.MessageID = message.MessageId;
                        singleResp.MD5OfBody = message.MD5OfBody;
                        singleResp.ReceiptHandle = message.ReceiptHandle;

                        resp.Add(singleResp);
                    }

                    //Delete the SQS message once get the SQS.
                    String messageRecieptHandle = receiveMessageResponse.Messages[0].ReceiptHandle;
                    DeleteMessageRequest deleteRequest = new DeleteMessageRequest();
                    deleteRequest.QueueUrl = sqsQueueURL;
                    deleteRequest.ReceiptHandle = messageRecieptHandle;
                    sqs.DeleteMessage(deleteRequest);

                    //Pause for 1 sec after message deleted.
                    Thread.Sleep(500);
                }
                else
                {
                    log.Info("[RPN_SQS] No message found. SQS worker sleep for 30 seconds");

                    //Pause for 30 seconds if no SQS message found
                    Thread.Sleep(30000);
                }
            }
            catch (AmazonSQSException ex)
            {
                log.Error("[RPN_SQS] Amazon Error encounted: " + ex.Message + ". Status Code: " + ex.StatusCode);
                resp = new List<SQSResponse>();
            }
            catch (Exception ex)
            {
                log.Error("[RPN_SQS] General Error encounted: " + ex.Message);
                resp = new List<SQSResponse>();
            }

            return resp;
        }
    }

    public class SQSResponse
    {
        public string MessageID { get; set; }
        public string ReceiptHandle { get; set; }
        public string MD5OfBody { get; set; }
        public string Body { get; set; }
    }

}
