﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Astro.RPN.Utilities
{
    public class SimpleEncryption
    {
        // configuration file path and section of connection string
        private string EncryptedPassword;
        private byte[] Key;
        private byte[] Vector;
        RijndaelManaged RM;
        ICryptoTransform Encryptor, Decryptor;

        public SimpleEncryption()
        {
            this.RM = new RijndaelManaged();
        }

        //create encryptor 
        private void createEncryptor()
        {
            this.Encryptor = RM.CreateEncryptor(Key, Vector);
        }

        //create decryptor
        private void createDecryptor()
        {
            this.Decryptor = RM.CreateDecryptor(Key, Vector);
        }

        public string Encrypt(string password)
        {
            this.Key = new byte[32] { 178, 235, 120, 148, 67, 194, 150, 72, 217, 172, 163, 108, 135, 149, 217, 73, 100, 217, 117, 94, 44, 36, 105, 170, 228, 153, 1, 163, 24, 241, 231, 128 };
            this.Vector = new byte[16] { 207, 219, 126, 181, 152, 249, 217, 101, 3, 117, 237, 196, 78, 91, 5, 91 };
            createEncryptor();
            //string output;
            byte[] outputData;

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter sr = new StreamWriter(cs))
                    {
                        sr.Write(password);
                    }
                    outputData = memoryStream.ToArray();
                }
            }

            EncryptedPassword = Convert.ToBase64String(outputData);
            return EncryptedPassword;
        }

        public string Decrypt(string encryptedpassword)
        {

            this.Key = new byte[32] { 178, 235, 120, 148, 67, 194, 150, 72, 217, 172, 163, 108, 135, 149, 217, 73, 100, 217, 117, 94, 44, 36, 105, 170, 228, 153, 1, 163, 24, 241, 231, 128 };
            this.Vector = new byte[16] { 207, 219, 126, 181, 152, 249, 217, 101, 3, 117, 237, 196, 78, 91, 5, 91 };
            createDecryptor();
            string output;
            //XmlNode node = GetConfig();
            byte[] data = Convert.FromBase64String(encryptedpassword);
            //byte[] outputData;
            using (MemoryStream memoryStream = new MemoryStream(data))
            {
                /*try
                {*/
                using (CryptoStream cs = new CryptoStream(memoryStream, Decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader sr = new StreamReader(cs))
                    {
                        //read 
                        output = sr.ReadToEnd();
                    }
                }
            }
            return output;
        }
    }
}
