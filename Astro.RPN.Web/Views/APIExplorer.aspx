﻿<%@ Page Title="RPN - API Exlorer" Language="C#" MasterPageFile="~/Views/RPN.master" AutoEventWireup="true" CodeFile="APIExplorer.aspx.cs" Inherits="Views_APIRunner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="apireq">
        <div class="cover">
            <span class="labels">Request URL</span>
            <asp:TextBox CssClass="txtbox" ID="txtRequestUrl" runat="server" spellcheck="false"></asp:TextBox>

            <div class="reqresp">
                <div class="req">
                    <span class="stdlabel">Request JSON</span>
                    <asp:TextBox CssClass="txtbox" ID="txtReqJson" runat="server" TextMode="MultiLine" spellcheck="false"></asp:TextBox>
                    <asp:Button CssClass="btn" ID="postReqBtn" runat="server" Text="Post request" OnClick="postReqBtn_Click" />
                </div>

                <div class="resp">
                    <span class="stdlabel">Response JSON</span>
                    <asp:TextBox CssClass="txtbox" ID="txtRespJson" runat="server" TextMode="MultiLine" spellcheck="false"></asp:TextBox>
                    <div class="resptime">
                        <asp:Label ID="lblRespTime" runat="server" Text="Response time : "></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

