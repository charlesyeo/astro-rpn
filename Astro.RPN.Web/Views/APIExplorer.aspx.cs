﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Views_APIRunner : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtRequestUrl.Text))
        {
            txtRequestUrl.Text = "http://rpn-dev-api-1047594470.ap-southeast-1.elb.amazonaws.com/checkaccountexists";
        }     
    }

    protected void postReqBtn_Click(object sender, EventArgs e)
    {
        string requestUrl = txtRequestUrl.Text;
        string requestJson = txtReqJson.Text;

        if (!string.IsNullOrEmpty(requestUrl) && !string.IsNullOrEmpty(requestUrl))
        {
            var startTime = DateTime.Now;

            var webClient = new System.Net.WebClient();
            webClient.Headers[System.Net.HttpRequestHeader.ContentType] = "text/json";

            string resp = webClient.UploadString(requestUrl, requestJson);

            var endTime = DateTime.Now;

            string respTime = "Response time  :  " + (DateTime.Now - startTime).Milliseconds + "ms";
            string formattedResp = BeautifyJsonString(resp);

            string minifiedReq = MinifyJsonString(requestJson);
            string formattedReq = BeautifyJsonString(minifiedReq);

            lblRespTime.Text = respTime;

            txtReqJson.Text = formattedReq;
            txtRespJson.Text = formattedResp;
        }
    }

    public static string MinifyJsonString(string jsonString)
    {
        return Regex.Replace(jsonString, "(\"(?:[^\"\\\\]|\\\\.)*\")|\\s+", "$1");
    }

    public static string BeautifyJsonString(string jsonString)
    {
        string INDENT_STRING = "    ";
        var indent = 0;
        var quoted = false;
        var sb = new StringBuilder();
        for (var i = 0; i < jsonString.Length; i++)
        {
            var ch = jsonString[i];
            switch (ch)
            {
                case '{':

                case '[':
                    sb.Append(ch);
                    if (!quoted)
                    {
                        sb.AppendLine();

                        int incIndent = ++indent;

                        for (int a = 0; a < incIndent; a++)
                        {
                            sb.Append(INDENT_STRING);
                        }
                    }
                    break;

                case '}':

                case ']':
                    if (!quoted)
                    {
                        sb.AppendLine();

                        int decIndent = --indent;

                        for (int a = 0; a < decIndent; a++)
                        {
                            sb.Append(INDENT_STRING);
                        }

                        //Enumerable.Range(0, --indent).ForEach(item => sb.Append(INDENT_STRING));
                    }
                    sb.Append(ch);
                    break;
                case '"':
                    sb.Append(ch);
                    bool escaped = false;
                    var index = i;
                    while (index > 0 && jsonString[--index] == '\\')
                        escaped = !escaped;
                    if (!escaped)
                        quoted = !quoted;
                    break;
                case ',':
                    sb.Append(ch);
                    if (!quoted)
                    {
                        sb.AppendLine();

                        for (int a = 0; a < indent; a++)
                        {
                            sb.Append(INDENT_STRING);
                        }

                        //Enumerable.Range(0, indent).ForEach(item => sb.Append(INDENT_STRING));
                    }
                    break;
                case ':':
                    sb.Append(ch);
                    if (!quoted)
                        sb.Append(" ");
                    break;
                default:
                    sb.Append(ch);
                    break;
            }
        }
        return sb.ToString();
    }
}