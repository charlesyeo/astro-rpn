﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astro.RPN.Web
{
    public class Utils
    {
        public static bool IsSignatureValid(string partnerKey, string sig, string prefix, string data, string postfix, string generateFrom = "default")
        {
            string givenSig = sig;
            string regeneratedSig = GenerateSignature(prefix, data, postfix);
            bool isSigValid = false;

            if (givenSig.Equals(regeneratedSig))
            {
                isSigValid = true;
            }

            return isSigValid;
        }

        public static bool IsCreatePaymentSigValid(string givenSig, DateTime timestamp, string partnerid, string bankclientrefid, string billacctnum, string amount, DateTime transdate, string tellerid, string branchid, string secretkey, string activitytype, string paymentmethod, string creditcardnumber, string ccexpirydate, string creditcardtype, string paymentsource)
        {
            bool isSigValid = false;

            string regeneratedSig = GenerateCreatePaymentSig(timestamp, partnerid, bankclientrefid, billacctnum, amount, transdate, tellerid, branchid, secretkey, activitytype, paymentmethod, creditcardnumber, ccexpirydate, creditcardtype, paymentsource);

            if (givenSig.Equals(regeneratedSig))
            {
                isSigValid = true;
            }

            return isSigValid;
        }

        public static string GenerateSignature(string prefix, string data, string postfix)
        {
            string rawSig = GenerateRawSignature(prefix, data, postfix);

            var oBytes = System.Text.Encoding.UTF8.GetBytes(rawSig);
            var oHash = new System.Security.Cryptography.SHA256Cng();

            var hashBytes = oHash.ComputeHash(oBytes);

            return System.Convert.ToBase64String(hashBytes);
        }

        public static string GenerateCreatePaymentSig(DateTime timestamp, string partnerid, string bankclientrefid, string billacctnum, string amount, DateTime transdate, string tellerid, string branchid, string secretkey, string activitytype, string paymentmethod, string creditcardnumber, string ccexpirydate, string creditcardtype, string paymentsource)
        {
            string concatenateparams = String.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}", timestamp.ToString("yyyyMMddHHmmss"), activitytype, amount, bankclientrefid, billacctnum, branchid, ccexpirydate, creditcardnumber, creditcardtype, paymentmethod, paymentsource, tellerid, transdate.ToString("yyyyMMddHHmmss"), secretkey);

            return CreateSHAHash(concatenateparams);
        }

        private static string CreateSHAHash(string phrase)
        {
            byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(phrase);
            byte[] hashedBytes = (new System.Security.Cryptography.SHA256CryptoServiceProvider()).ComputeHash(inputBytes);

            return Convert.ToBase64String(hashedBytes);
        }

        public static string GenerateRawSignature(string prefix, string data, string postfix)
        {
            var sb = new System.Text.StringBuilder();
            sb.Append(prefix);
            sb.Append(data);
            sb.Append(postfix);

            return sb.ToString();
        }

        public static decimal CheckDigit(String acctNo)
        {
            int acctnumber = int.Parse(acctNo);
            int total = 0;

            for (int i = 9; i > 0; i--)
            {
                total += (acctnumber % 10) * (11 - i);
                acctnumber = Convert.ToInt32(Decimal.Truncate(acctnumber / 10));
            }

            int a = total % 11;

            if (a >= 10)
                a = 0;

            return a;
        }
    }
}
