﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Astro.RPN.Web.Controller
{
    public class PaymentController: ApiController
    {
        [Route("checkaccountexists"), AcceptVerbs("POST")]
        public Model.PaymentModel.CheckAccountExistsResponse CheckAccountExists([FromBody] Model.PaymentModel.CheckAccountExistsRequest req)
        {
            var results = new Model.PaymentModel.CheckAccountExistsResponse();
            var timeStamp = DateTime.UtcNow;

            try
            {
                // Checks if partner ID has value
                if (string.IsNullOrEmpty(req.body.partnerid) || string.IsNullOrEmpty(req.siginfo.sig))
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "200";
                    results.siginfo.responsemessage = "PartnerID parameter or Sig parameter passed as input parameter is invalid; or PartnerID has not been granted access to call this function";
                    return results;
                }

                // Checks if partner status is valid
                int partnerStatus = Astro.RPN.Partners.Queries.GetPartnerStatus(req.body.partnerid);

                if (partnerStatus != 1)
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "200";
                    results.siginfo.responsemessage = "PartnerID parameter or Sig parameter passed as input parameter is invalid; or PartnerID has not been granted access to call this function";
                    return results;
                }

                // Checks if mandatory fields are missing
                if (string.IsNullOrEmpty(req.body.astroaccountid) || string.IsNullOrEmpty(req.siginfo.timestamp))
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "201";
                    results.siginfo.responsemessage = "Invalid input parameter values received";
                    return results;
                }

                // Checks if partner secret key exist
                string partnerSecretKey = Astro.RPN.Partners.Queries.GetSecretKey(req.body.partnerid);

                if (string.IsNullOrEmpty(partnerSecretKey))
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "203";
                    results.siginfo.responsemessage = "Partner password not found.";
                    return results;
                }

                // Checks if signature is valid
                string realTimestamp = Convert.ToDateTime(req.siginfo.timestamp).ToUniversalTime().ToString("yyyyMMddHHmmss");

                bool isSigValid = Astro.RPN.Web.Utils.IsSignatureValid(req.body.partnerid, req.siginfo.sig, realTimestamp, req.body.GetSignatureString(), partnerSecretKey);

                if (!isSigValid)
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "202";
                    results.siginfo.responsemessage = "Signature not match";
                    return results;
                }     

                results = Astro.RPN.Web.Logic.Payment.CheckAccountExists(req);
            }
            catch (Exception ex)
            {
                results.siginfo.result = false;
                results.siginfo.responsecode = "901";
                results.siginfo.responsemessage = ex.Message + ". Web Service application error. Please contact administrator."; ;
            }

            return results;
        }

        [Route("createpayment"), AcceptVerbs("POST")]
        public Model.PaymentModel.CreatePaymentResponse CreatePayment([FromBody] Model.PaymentModel.CreatePaymentRequest req)
        {
            var results = new Model.PaymentModel.CreatePaymentResponse();
            var timeStamp = DateTime.UtcNow;

            try
            {
                // Checks if partner ID has value
                if (string.IsNullOrEmpty(req.body.partnerid))
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "200";
                    results.siginfo.responsemessage = "PartnerID parameter or Sig parameter passed as input parameter is invalid; or PartnerID has not been granted access to call this function";
                    return results;
                }

                // Checks if partner status is valid
                int partnerStatus = Astro.RPN.Partners.Queries.GetPartnerStatus(req.body.partnerid);

                if (partnerStatus != 1)
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "200";
                    results.siginfo.responsemessage = "PartnerID parameter or Sig parameter passed as input parameter is invalid; or PartnerID has not been granted access to call this function";
                    return results;
                }

                // Checks if partner secret key exist
                string partnerSecretKey = Astro.RPN.Partners.Queries.GetSecretKey(req.body.partnerid);

                if (string.IsNullOrEmpty(partnerSecretKey))
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "203";
                    results.siginfo.responsemessage = "Partner password not found";
                    return results;
                }

                // Checks if signature is valid
                DateTime realTimestamp = Convert.ToDateTime(req.siginfo.timestamp).ToUniversalTime();
                DateTime tranxRealTimestamp = Convert.ToDateTime(req.body.transdate);

                bool isSigValid = Astro.RPN.Web.Utils.IsCreatePaymentSigValid(req.siginfo.sig, realTimestamp, req.body.partnerid, req.body.bankclientrefid, req.body.billacctnum, req.body.amount, tranxRealTimestamp, req.body.tellerid, req.body.branchid, partnerSecretKey, req.body.activitytype, req.body.paymentmethod, req.body.creditcardnumber, req.body.ccexpirydate, req.body.creditcardtype, req.body.paymentsource);

                if (!isSigValid)
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "202";
                    results.siginfo.responsemessage = "Signature not match";
                    return results;
                }

                // Checks if mandatory fields are missing
                if (string.IsNullOrEmpty(req.body.bankclientrefid) || string.IsNullOrEmpty(req.body.billacctnum) || string.IsNullOrEmpty(req.body.amount) || string.IsNullOrEmpty(req.body.transdate))
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "201";
                    results.siginfo.responsemessage = "Invalid input parameter values received";
                    return results;
                }

                results = Astro.RPN.Web.Logic.Payment.CreatePayment(req);
            }
            catch (Exception ex)
            {
                results.siginfo.result = false;
                results.siginfo.responsecode = "901";
                results.siginfo.responsemessage = ex.Message + ". Web Service application error. Please contact administrator."; ;
                return results;
            }

            return results;
        }

        [Route("createmyclearpayment"), AcceptVerbs("POST")]
        public Model.PaymentModel.CreateMyClearPaymentResponse CreateMyClearPayment([FromBody] Model.PaymentModel.CreateMyclearPaymentRequest req)
        {
            var results = new Model.PaymentModel.CreateMyClearPaymentResponse();
            var timeStamp = DateTime.UtcNow;

            string responseResultNo = "N";
            string responseResultYes = "Y";

            string partnerID = "myclear";

            try
            {
                // Checks if headers are empty
                if (String.IsNullOrEmpty(req.header.sig) || String.IsNullOrEmpty(req.header.timestamp))
                {
                    results.body.result = responseResultNo;
                    results.body.errorcode = "300";
                    results.body.errormessage = "Timestamp or sig parameter passed as input parameter is invalid";
                    results.body.resend = responseResultYes;
                    return results;
                }

                // Checks if 15 mins delayed
                var timestamp = Convert.ToDateTime(req.header.timestamp).ToUniversalTime();
                var timestampNow = DateTime.UtcNow;

                if (timestamp.AddMinutes(15) < timestampNow)
                {
                    results.body.result = responseResultNo;
                    results.body.errorcode = "301";
                    results.body.errormessage = "Timestamp is greater than 15 minutes. Transaction rejected.";
                    results.body.resend = responseResultYes;
                    return results;
                }

                // Checks all mandatory fields
                if (string.IsNullOrEmpty(req.body.nbpsref))
                {
                    results.body.result = responseResultNo;
                    results.body.errorcode = "311";
                    results.body.errormessage = "Invalid nbpsref parameter.";
                    results.body.resend = responseResultYes;
                    return results;
                }

                if (string.IsNullOrEmpty(req.body.payerbanknum) || string.IsNullOrEmpty(req.body.payerbankname))
                {
                    results.body.result = responseResultNo;
                    results.body.errorcode = "302";
                    results.body.errormessage = "Invalid payerbanknum/payerbankname parameters.";
                    results.body.resend = responseResultYes;
                    return results;
                }

                if (string.IsNullOrEmpty(req.body.billerbanknum) || string.IsNullOrEmpty(req.body.billerbankname))
                {
                    results.body.result = responseResultNo;
                    results.body.errorcode = "303";
                    results.body.errormessage = "Invalid billerbanknum/billerbankname parameters.";
                    results.body.resend = responseResultYes;
                    return results;
                }

                if (string.IsNullOrEmpty(req.body.accounttype))
                {
                    results.body.result = responseResultNo;
                    results.body.errorcode = "304";
                    results.body.errormessage = "Invalid accounttype parameters.";
                    results.body.resend = responseResultYes;
                    return results;
                }

                if (string.IsNullOrEmpty(req.body.billercode) || string.IsNullOrEmpty(req.body.billercodename))
                {
                    results.body.result = responseResultNo;
                    results.body.errorcode = "305";
                    results.body.errormessage = "Invalid billercode/billercodename parameters.";
                    results.body.resend = responseResultYes;
                    return results;
                }

                if (string.IsNullOrEmpty(req.body.channel))
                {
                    results.body.result = responseResultNo;
                    results.body.errorcode = "306";
                    results.body.errormessage = "Invalid channel parameters.";
                    results.body.resend = responseResultYes;
                    return results;
                }

                if (string.IsNullOrEmpty(req.body.debittimestamp))
                {
                    results.body.result = responseResultNo;
                    results.body.errorcode = "307";
                    results.body.errormessage = "Invalid debittimestamp parameters.";
                    results.body.resend = responseResultYes;
                    return results;
                }

                if (string.IsNullOrEmpty(req.body.rrn))
                {
                    results.body.result = responseResultNo;
                    results.body.errorcode = "308";
                    results.body.errormessage = "Invalid rrn parameters.";
                    results.body.resend = responseResultYes;
                    return results;
                }

                if (string.IsNullOrEmpty(req.body.currencycode))
                {
                    results.body.result = responseResultNo;
                    results.body.errorcode = "309";
                    results.body.errormessage = "Invalid currencycode parameters.";
                    results.body.resend = responseResultYes;
                    return results;
                }

                if (req.body.amount <= 0)
                {
                    results.body.result = responseResultNo;
                    results.body.errorcode = "310";
                    results.body.errormessage = "Amount must be greater than 0.";
                    results.body.resend = responseResultYes;
                    return results;
                }

                // Checks partner secret key
                string partnerSecretKey = Astro.RPN.Partners.Queries.GetSecretKey(partnerID);

                if (string.IsNullOrEmpty(partnerSecretKey))
                {
                    results.body.result = responseResultNo;
                    results.body.errorcode = "203";
                    results.body.errormessage = "Partner password not found.";
                    results.body.resend = responseResultYes;
                    return results;
                }

                // Checks if signature is valid
                string realTimestamp = Convert.ToDateTime(req.header.timestamp).ToUniversalTime().ToString("yyyyMMddHHmmss");

                bool isSigValid = Astro.RPN.Web.Utils.IsSignatureValid(partnerID, req.header.sig, realTimestamp, req.body.GetSignatureString(), partnerSecretKey);

                if (!isSigValid)
                {
                    results.body.result = responseResultNo;
                    results.body.errorcode = "202";
                    results.body.errormessage = "Signature not match";
                    results.body.resend = responseResultYes;
                    return results;
                }               

                results = Astro.RPN.Web.Logic.Payment.CreateMyClearPayment(req);
            }
            catch (Exception ex)
            {
                results.body.result = responseResultNo;
                results.body.errorcode = "901";
                results.body.errormessage = ex.Message + ". Web Service application error. Please contact administrator."; ;
                results.body.resend = responseResultYes;
                return results;
            }

            return results;
        }
    }
}