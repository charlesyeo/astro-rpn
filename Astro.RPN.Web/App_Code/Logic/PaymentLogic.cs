﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using System.Configuration;

namespace Astro.RPN.Web.Logic
{
    public class Payment
    {
        public static Model.PaymentModel.CheckAccountExistsResponse CheckAccountExists(Model.PaymentModel.CheckAccountExistsRequest req)
        {
            var results = new Model.PaymentModel.CheckAccountExistsResponse();

            try
            {      
                // Check is Astro account ID is valid
                int accID = 0;

                if (!Int32.TryParse(req.body.astroaccountid, out accID))
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "201";
                    results.siginfo.responsemessage = "Wrong data type and expecting integer only for astroaccountid";
                    return results;
                }

                // Checks if astro account ID is exist and valid
                Astro.RPN.Database.CusInfo.paymentgw_accountinfo accountInfo = Astro.RPN.Payment.Queries.GetAccountInfo(req.body.astroaccountid);

                if (accountInfo == null)
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "103";
                    results.siginfo.responsemessage = "No details found! (Astro Account Number not found)";
                    return results;
                }

                // Checks and ammends astro account ID
                string accIDStr = String.Empty;

                if (req.body.astroaccountid.Length == 10)
                {
                    accIDStr = req.body.astroaccountid.Substring(1, 8);
                    accIDStr = "0" + accIDStr + Astro.RPN.Web.Utils.CheckDigit(accIDStr);
                }           

                else if (req.body.astroaccountid.Length == 9)
                {
                    accIDStr = req.body.astroaccountid.Substring(0, 8);
                    accIDStr = accIDStr + Astro.RPN.Web.Utils.CheckDigit(accIDStr);
                }
                else if (req.body.astroaccountid.Length == 8)
                    accIDStr = req.body.astroaccountid;

                else
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "204";
                    results.siginfo.responsemessage = "Invalid astroaccountid length";
                    return results;
                }

                string tendigitAccountID = "0" + accIDStr + Astro.RPN.Web.Utils.CheckDigit(accIDStr);

                // Gets payment info
                Astro.RPN.Database.CusInfo.aspmybill paymentBill = Astro.RPN.Payment.Queries.GetPaymentBill(req.body.astroaccountid);
                
                if (paymentBill != null)
                {
                    string outstandingBalanceStr = paymentBill.total_amount_due.Replace("RM", "").Replace(" ", "");
                    string lastPaymentAmountStr = paymentBill.payment.Replace("RM", "").Replace(" ", "");

                    Decimal outstandingBalance;

                    if (!Decimal.TryParse(outstandingBalanceStr, out outstandingBalance))
                    {
                        results.siginfo.result = false;
                        results.siginfo.responsecode = "202";
                        results.siginfo.responsemessage = "Invalid outstanding balance.";
                        return results;
                    }

                    Decimal lastPaymentAmount;

                    if (!Decimal.TryParse(lastPaymentAmountStr, out lastPaymentAmount))
                    {
                        results.siginfo.result = false;
                        results.siginfo.responsecode = "203";
                        results.siginfo.responsemessage = "Invalid last payment amount.";
                        return results;
                    }

                    string billStatementDateStr = "";

                    if (!string.IsNullOrEmpty(paymentBill.bill_date))
                    {
                        DateTime billStatementDate;
                        DateTime.TryParseExact(paymentBill.bill_date, "dd/MM/yy", CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces, out billStatementDate);

                        billStatementDateStr = billStatementDate.ToString("yyyyMMdd");
                    }

                    results.body.accountname = accountInfo.name;
                    results.body.accountid = paymentBill.astro_account_number;
                    results.body.outstandingbalance = outstandingBalance;
                    results.body.lastpaymentamount = lastPaymentAmount;
                    results.body.statementdate = billStatementDateStr;
                    results.siginfo.result = true;
                    results.siginfo.responsecode = "100";
                    results.siginfo.responsemessage = "Success. Account exists.";
                }
                else
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "201";
                    results.siginfo.responsemessage = "No payment bill found.";
                    return results;
                }
            }
            catch (Exception ex)
            {
                results.siginfo.result = false;
                results.siginfo.responsecode = "901";
                results.siginfo.responsemessage = ex.Message + ". Web Service application error. Please contact administrator";
            }

            return results;
        }

        public static Model.PaymentModel.CreatePaymentResponse CreatePayment(Model.PaymentModel.CreatePaymentRequest req)
        {
            var results = new Model.PaymentModel.CreatePaymentResponse();

            // Checks if payment method is empty and is either : Credit card (CC), Cash (CA), 
            try
            {
                if (string.IsNullOrEmpty(req.body.paymentmethod) || (req.body.paymentmethod != "CC" && req.body.paymentmethod != "CA" && req.body.paymentmethod != "DD"))
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "207";
                    results.siginfo.responsemessage = "Invalid paymentmethod value received";
                    return results;
                }

                if (string.IsNullOrEmpty(req.body.creditcardnumber) || string.IsNullOrEmpty(req.body.ccexpirydate) || string.IsNullOrEmpty(req.body.creditcardtype))
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "208";
                    results.siginfo.responsemessage = "Invalid credit card parameters value received";
                    return results;
                }

                // Checks if activity type is valid
                if (string.IsNullOrEmpty(req.body.activitytype) || (req.body.activitytype != Model.Enums.ActivityType.AR.ToString() && req.body.activitytype != Model.Enums.ActivityType.CM.ToString()))
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "209";
                    results.siginfo.responsemessage = "Invalid activitytype value received";
                    return results;
                }

                // Checks if system type is valid
                string systemType = "";

                if (string.IsNullOrEmpty(req.body.systemtype))
                {
                    systemType = Model.Enums.SystemType.RPN.ToString().ToLower();
                }
                else
                {
                    if (req.body.systemtype != Model.Enums.SystemType.APG.ToString().ToLower() && req.body.systemtype != Model.Enums.SystemType.RPN.ToString().ToLower())
                    {
                        results.siginfo.result = false;
                        results.siginfo.responsecode = "212";
                        results.siginfo.responsemessage = "Invalid systemtype value received";
                        return results;
                    }
                }

                // Checks if bill account number type and length are valid
                int accID = 0;

                if (!Int32.TryParse(req.body.billacctnum, out accID))
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "206";
                    results.siginfo.responsemessage = "Invalid billacctnum length";
                    return results;
                }

                string accIDStr = String.Empty;

                if (req.body.billacctnum.ToString().Length == 10)
                    accIDStr = req.body.billacctnum.ToString().Substring(1, 8);

                else if (req.body.billacctnum.ToString().Length == 9)
                    accIDStr = req.body.billacctnum.ToString().Substring(0, 8);

                else if (req.body.billacctnum.ToString().Length == 8)
                    accIDStr = req.body.billacctnum.ToString();

                else
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "206";
                    results.siginfo.responsemessage = "Invalid billacctnum length";
                    return results;
                }

                // Checks if bank sends a duplicated ref ID
                bool isQueuedBefore = Astro.RPN.Queue.Queries.CheckIfQueued(req.body.partnerid, req.body.bankclientrefid);

                if (isQueuedBefore)
                {
                    results.siginfo.result = false;
                    results.siginfo.responsecode = "210";
                    results.siginfo.responsemessage = "Duplicate BankClientRefId found.";
                    return results;
                }

                string queueMessageBody = String.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}|{17}",
                                            req.body.partnerid, 
                                            req.body.bankclientrefid,
                                            accIDStr, 
                                            req.body.amount, 
                                            req.body.transdate, 
                                            req.body.transstatus, 
                                            req.body.tellerid, 
                                            req.body.branchid,
                                            req.body.activitytype, 
                                            String.Empty, 
                                            req.body.paymentmethod,
                                            (req.body.systemtype == Model.Enums.SystemType.RPN.ToString().ToLower()) ? Model.Enums.SystemType.RPN : Model.Enums.SystemType.APG,
                                            req.body.creditcardnumber, 
                                            req.body.ccexpirydate, 
                                            req.body.creditcardtype, 
                                            req.body.paymentsource, 
                                            req.body.interactionchannel, 
                                            req.body.partneruserkey);

                string sqsUrl = ConfigurationManager.AppSettings["RPN.SQS.Url"];

                var sqsEngine = new Astro.RPN.Utilities.RPNSQS(sqsUrl, "");

                bool isSendToSQS = sqsEngine.QueueWithEncryptedSQS(queueMessageBody);

                if (isSendToSQS)
                {
                    DateTime tranxDate = Convert.ToDateTime(req.body.transdate);
                    Astro.RPN.Queue.Queries.LogQueue(req.body.partnerid, req.body.bankclientrefid, queueMessageBody, req.body.transstatus, tranxDate, 0, "N");

                    results.siginfo.result = true;
                    results.siginfo.responsecode = "100";
                    results.siginfo.responsemessage = "Success.";
                    results.body.bankclientrefid = req.body.bankclientrefid;

                }
                else
                {
                    // Will definitely need a backup plan if SQS fails other than log and return
                    results.siginfo.result = true;
                    results.siginfo.responsecode = "211";
                    results.siginfo.responsemessage = "Failed to queue the transaction. Please retry again later.";
                }
            }
            catch (Exception ex)
            {
                results.siginfo.result = true;
                results.siginfo.responsecode = "901";
                results.siginfo.responsemessage = ex.Message + ". Web Service application error. Please contact administrator.";
            }

            return results;
        }

        public static Model.PaymentModel.CreateMyClearPaymentResponse CreateMyClearPayment(Model.PaymentModel.CreateMyclearPaymentRequest req)
        {
            var results = new Model.PaymentModel.CreateMyClearPaymentResponse();

            string partnerID = "myclear";
            string responseResultNo = "N";
            string responseResultYes = "Y";

            try
            {    
                string accIDStr = string.Empty;

                if (req.body.rrn.Length == 10)
                    accIDStr = req.body.rrn.Substring(1, 8);

                else if (req.body.rrn.Length == 9)
                    accIDStr = req.body.rrn.Substring(0, 8);

                else if (req.body.rrn.ToString().Length == 8)
                    accIDStr = req.body.rrn.ToString();

                else
                {
                    results.body.result = responseResultNo;
                    results.body.errorcode = "311";
                    results.body.errormessage = "RRN must be 10-digits.";
                    results.body.resend = responseResultYes;
                    return results;
                }

                int accID = 0;

                if (!Int32.TryParse(accIDStr, out accID))
                {
                    results.body.result = responseResultNo;
                    results.body.errorcode = "311";
                    results.body.errormessage = "RRN must be integer.";
                    results.body.resend = responseResultYes;
                    return results;
                }

                bool isQueuedBefore = Astro.RPN.Queue.Queries.CheckIfQueued(partnerID, req.body.nbpsref);

                if (isQueuedBefore)
                {
                    results.body.result = responseResultNo;
                    results.body.errorcode = "313";
                    results.body.errormessage = "Duplicate NBPS ref id found.";
                    results.body.resend = responseResultYes;
                    return results;
                }

                string queueMessageBody = String.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}|{13}|{14}|{15}|{16}",
                                            partnerID, 
                                            req.body.payerbanknum, 
                                            req.body.payerbankname, 
                                            req.body.billerbanknum, 
                                            req.body.billerbankname, 
                                            req.body.accounttype, 
                                            req.body.billercode, 
                                            req.body.billercodename, 
                                            req.body.nbpsref,
                                            req.body.channel, 
                                            req.body.debittimestamp, 
                                            accIDStr, 
                                            req.body.currencycode, 
                                            req.body.amount, 
                                            req.body.repeatmsg, 
                                            req.body.rrn2, 
                                            req.body.extdata);
                
                //string queueMessageBody = Newtonsoft.Json.JsonConvert.SerializeObject(req);
                string sqsUrl = ConfigurationManager.AppSettings["RPN.SQS.MyClear.Url"];

                var sqsEngine = new Astro.RPN.Utilities.RPNSQS(sqsUrl, "");

                bool isSendToSQS = sqsEngine.QueueWithEncryptedSQS(queueMessageBody);

                if (isSendToSQS)
                {
                    DateTime tranxDate = Convert.ToDateTime(req.body.debittimestamp);
                    Astro.RPN.Queue.Queries.LogQueue(partnerID, req.body.nbpsref, queueMessageBody, "", tranxDate, 0, "N");

                    results.body.result = responseResultYes;
                    results.body.errorcode = "100";
                    results.body.errormessage = "Success.";
                    results.body.resend = responseResultNo;
                }
                else
                {
                    // Will definitely need a backup plan if SQS fails
                    results.body.result = responseResultNo;
                    results.body.errorcode = "312";
                    results.body.errormessage = "Failed to queue the transaction. Please retry again later.";
                    results.body.resend = responseResultYes;
                }
            }
            catch (Exception ex)
            {
                results.body.result = responseResultNo;
                results.body.errorcode = "901";
                results.body.errormessage = ex.Message + ". Web Service application error. Please contact administrator.";
                results.body.resend = responseResultYes;
            }

            return results;
        }
    }
}