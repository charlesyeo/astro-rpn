﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Astro.RPN.Database;
using Astro.RPN.Database.CusInfo;

namespace Astro.RPN.Queue
{
    public class Queries
    {
        public static bool CheckIfQueued(string partnerID, string bankRefID)
        {    
            bool isQueued = false;

            using (var rpnDB = new PetaPoco.Database("RPNDBConnectionString"))
            {
                var sqsQueueJob =  (rpnDB.Query<sqs_queuedjob>(PetaPoco.Sql.Builder
                                    .Append("WHERE partner_id =@0", partnerID)
                                    .Append("AND bank_reference_id =@0", bankRefID))).SingleOrDefault();

                if (sqsQueueJob != null)
                {
                    isQueued = true;
                }
            }

            return isQueued;
        }

        public static bool LogQueue(string partnerID, string bankRefID, string queueMessageBody, string tranxStatus, DateTime tranxDate, int sqsStatus, string status)
        {
            bool isLogged = false;

            try
            {
                using (var rpnDB = new PetaPoco.Database("RPNDBConnectionString"))
                {
                    var sqsQueueJob = new sqs_queuedjob()
                    {
                        partner_id = partnerID,
                        bank_reference_id = bankRefID,
                        request = queueMessageBody,
                        transaction_status = tranxStatus,
                        transaction_date = tranxDate,
                        sqs_status = Convert.ToInt16(sqsStatus),
                        status = status,
                        date_inserted = DateTime.Now
                    };

                    rpnDB.Insert(sqsQueueJob);

                    isLogged = true;
                }
            }
            catch(Exception ex)
            {
                isLogged = false;
            }

            return isLogged;
        }
    }
}