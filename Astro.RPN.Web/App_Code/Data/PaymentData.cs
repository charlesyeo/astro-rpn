﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Astro.RPN.Database.CusInfo;

namespace Astro.RPN.Payment
{
    public class Queries
    {
        public static paymentgw_accountinfo GetAccountInfo(string astroAccountID)
        {
            var results = new paymentgw_accountinfo();

            using (var rpnDB = new PetaPoco.Database("RPNCustInfoDBConnectionString"))
            {
                try
                {
                    results = (rpnDB.Query<paymentgw_accountinfo>(PetaPoco.Sql.Builder
                               .Append("WHERE accountno =@0", astroAccountID))).SingleOrDefault();
                }
                catch (Exception ex)
                {
                    // Log error
                }
            }

            return results;
        }

        public static aspmybill GetPaymentBill(string astroAccountID)
        {
            using (var rpnDB = new PetaPoco.Database("RPNCustInfoDBConnectionString"))
            {
                return (rpnDB.Query<aspmybill>(PetaPoco.Sql.Builder
                           .Append("WHERE astro_account_number =@0", astroAccountID))).SingleOrDefault();
            }
        }

    }
}