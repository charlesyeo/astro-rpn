﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace BaseConfig
{
    public class RouteConfig
    {
        public static void Register(RouteCollection routes)
        {
            routes.RouteExistingFiles = false;

            //Main routes
            routes.MapPageRoute(
                "API runner page",
                "views/api-explorer",
                "~/views/APIExplorer.aspx"
            );
        }
    }
}