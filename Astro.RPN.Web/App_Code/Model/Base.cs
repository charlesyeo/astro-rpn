﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astro.RPN.Web.Model
{
    public class Base
    {
        public class RequestHeader
        {
            public string sig { get; set; }
            public string timestamp { get; set; }
        }

        public class ResponseHeader
        {
            public string sig { get; set; }
            public string timestamp { get; set; }
            public bool result { get; set; }
            public string responsecode { get; set; }
            public string responsemessage { get; set; }
        }


    }
}