﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astro.RPN.Web.Model
{
    public class PaymentModel
    {
        #region CheckAccountExists

        //Request
        public class CheckAccountExistsRequest
        {
            public Model.Base.RequestHeader siginfo = new Model.Base.RequestHeader();
            public CheckAccountExistsRequestBody body = new CheckAccountExistsRequestBody();

            public class CheckAccountExistsRequestBody
            {
                public string astroaccountid { get; set; }
                public string partnerid { get; set; }

                public string GetSignatureString()
                {
                    var sb = new System.Text.StringBuilder();
                    sb.Append(astroaccountid);
                    //sb.Append(partnerid); remove for unknown
                    return sb.ToString();
                }
            }
        }

        //Response
        public class CheckAccountExistsResponse
        {
            public Model.Base.ResponseHeader siginfo = new Model.Base.ResponseHeader();
            public CheckAccountExistsResponseBody body = new CheckAccountExistsResponseBody();

            public class CheckAccountExistsResponseBody
            {
                public string accountid { get; set; }
                public string accountname { get; set; }
                public decimal lastpaymentamount { get; set; }
                public decimal outstandingbalance { get; set; }
                public string statementdate { get; set; }

                public string GetSignatureString()
                {
                    var sb = new System.Text.StringBuilder();
                    sb.Append(accountid);
                    sb.Append(accountname);
                    sb.Append(lastpaymentamount);
                    sb.Append(outstandingbalance);
                    sb.Append(statementdate);
                    return sb.ToString();
                }
            }
        }

        #endregion

        #region CreatePayment

        //Request
        public class CreatePaymentRequest
        {
            public Model.Base.RequestHeader siginfo = new Model.Base.RequestHeader();
            public CreatePaymentRequestBody body = new CreatePaymentRequestBody();

            public class CreatePaymentRequestBody
            {
                public string activitytype { get; set; }
                public string amount { get; set; }
                public string bankclientrefid { get; set; }
                public string billacctnum { get; set; }
                public string branchid { get; set; }
                public string ccexpirydate { get; set; }
                public string creditcardnumber { get; set; }
                public string creditcardtype { get; set; }
                public string interactionchannel { get; set; }
                public string partnerid { get; set; }
                public string partneruserkey { get; set; }
                public string paymentmethod { get; set; }
                public string paymentsource { get; set; }
                public string systemtype { get; set; }
                public string transdate { get; set; }
                public string transstatus { get; set; }
                public string tellerid { get; set; }

                public string GetSignatureString()
                {
                    var sb = new System.Text.StringBuilder();
                    sb.Append(activitytype);
                    sb.Append(amount);
                    sb.Append(bankclientrefid);
                    sb.Append(billacctnum);
                    sb.Append(branchid);
                    sb.Append(ccexpirydate);
                    sb.Append(creditcardnumber);
                    sb.Append(creditcardtype);
                    sb.Append(paymentmethod);
                    sb.Append(paymentsource);
                    sb.Append(tellerid);
                    sb.Append(transdate);
                    return sb.ToString();
                }
            }
        }

        //Response
        public class CreatePaymentResponse
        {
            public Model.Base.ResponseHeader siginfo = new Model.Base.ResponseHeader();
            public CreatePaymentResponseBody body = new CreatePaymentResponseBody();

            public class CreatePaymentResponseBody
            {
                public string bankclientrefid { get; set; }

                public string GetSignatureString()
                {
                    var sb = new System.Text.StringBuilder();
                    sb.Append(bankclientrefid);
                    return sb.ToString();
                }
            }
        }

        #endregion

        #region CreateMyClearPayment

        //Request
        public class CreateMyclearPaymentRequest
        {
            public Model.Base.RequestHeader header = new Model.Base.RequestHeader();
            public CreateMyClearPaymentRequestBody body = new CreateMyClearPaymentRequestBody();

            public class CreateMyClearPaymentRequestBody
            {
                public string accounttype { get; set; }
                public long amount { get; set; }
                public string billerbankname { get; set; }
                public string billerbanknum { get; set; }
                public string billercode { get; set; }
                public string billercodename { get; set; }
                public string channel { get; set; }
                public string currencycode { get; set; }
                public string debittimestamp { get; set; }
                public string extdata { get; set; }
                public string nbpsref { get; set; }
                public string payerbankname { get; set; }
                public string payerbanknum { get; set; }
                public string repeatmsg { get; set; }
                public string rrn { get; set; }
                public string rrn2 { get; set; }

                public string GetSignatureString()
                {
                    var sb = new System.Text.StringBuilder();
                    sb.Append(accounttype);
                    sb.Append(amount);
                    sb.Append(billerbankname);
                    sb.Append(billerbanknum);
                    sb.Append(billercode);
                    sb.Append(billercodename);
                    sb.Append(channel);
                    sb.Append(currencycode);
                    sb.Append(debittimestamp);
                    sb.Append(extdata);
                    sb.Append(nbpsref);
                    sb.Append(payerbankname);
                    sb.Append(payerbanknum);
                    sb.Append(repeatmsg);
                    sb.Append(rrn);
                    sb.Append(rrn2);
                    return sb.ToString();
                }
            }
        }

        //Response
        public class CreateMyClearPaymentResponse
        {
            public Model.Base.ResponseHeader siginfo = new Model.Base.ResponseHeader();
            public CreateMyClearPaymentResponseBody body = new CreateMyClearPaymentResponseBody();

            public class CreateMyClearPaymentResponseBody
            {
                public string errorcode { get; set; }
                public string errormessage { get; set; }
                public string resend { get; set; }
                public string result { get; set; }

                public string GetSignatureString()
                {
                    var sb = new System.Text.StringBuilder();
                    sb.Append(errorcode);
                    sb.Append(errormessage);
                    sb.Append(resend);
                    sb.Append(result);
                    return sb.ToString();
                }
            }
        }

        #endregion
    }
}