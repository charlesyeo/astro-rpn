﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Astro.RPN.Web.Model
{
    public class Enums
    {
        public enum ActivityType
        {
            CM,
            AR
        }

        public enum SystemType
        {
            RPN,
            APG
        }
    }
}