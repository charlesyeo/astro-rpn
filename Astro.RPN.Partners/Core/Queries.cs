﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Astro.RPN.Database;

namespace Astro.RPN.Partners
{
    public class Queries
    {
        public static int GetPartnerStatus(string partnerID)
        {
            int status = 0;

            using (var rpnDB = new PetaPoco.Database("RPNDBConnectionString"))
            {
                var results = (rpnDB.Query<partner_partner>(PetaPoco.Sql.Builder
                                   .Append("WHERE partner_id =@0", partnerID)
                                   .Append("LIMIT 1"))).SingleOrDefault();

                if (results != null)
                {
                    status = Convert.ToInt32(results.status);
                }
            }

            return status;
        }

        public static string GetSecretKey(string partnerID)
        {
            string secretkey = String.Empty;

            using (var rpnDB = new PetaPoco.Database("RPNDBConnectionString"))
            {
                var results = (rpnDB.Query<partner_partner>(PetaPoco.Sql.Builder
                               .Append("WHERE partner_id =@0", partnerID)
                               .Append("LIMIT 1"))).SingleOrDefault();

                if (results != null)
                {
                    secretkey = results.secret_key;
                }
            }

            return secretkey;
        }
    }
}
